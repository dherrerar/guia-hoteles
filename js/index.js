$(function(){
    $("[data-bs-toggle='tooltip']").tooltip();
    $("[data-bs-toggle='popover']").popover();
    $('.carousel').carousel({
        interval:4000
    });

    $('#exampleModal').on("show.bs.modal", function (e){
        console.log("el modal contacto se esta mostrando");
        $('contactoBtn').removeClass('btn-outline-success');
        $('contactoBtn').addClass('btn-primary');
        $('contactoBtn').prop('disabled', true);
    });
    $('#exampleModal').on("show.bs.modal", function (e){
        console.log("el modal contacto se mostró");
    });
    $('#exampleModal').on("show.bs.modal", function (e){
        console.log("el modal contacto se oculta");
    });
    $('#exampleModal').on("show.bs.modal", function (e){
        console.log("el modal contacto se ocultó");
        $('#contactoBts').prop('disabled', false);
    });
});